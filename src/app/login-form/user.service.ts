import {Injectable} from '@angular/core';
import {User} from './user';
@Injectable()
export class UserService {
    user: User = {
        name: '',
        password: '',
        isLoggedIn: false
      };


    getName(): string{
        return this.user.name;
    }

    setName(name: string){
        this.user.name = name;
    }

    setIsLoggedIn(is: boolean){
        this.user.isLoggedIn = is;
    }

    ifLoggedIn() : boolean{
        return this.user.isLoggedIn;
    }
}
