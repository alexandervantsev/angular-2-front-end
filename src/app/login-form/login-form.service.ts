import { Injectable } from '@angular/core';
import { User} from "./user";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from "rxjs/Observable";
@Injectable()
export class LoginFormService {

  constructor(private http: HttpClient) {}

  
  addUser(user:User):Observable<any> {
    const url = 'http://localhost:8080/start/adduser';
    
    return this.http.post(url, user, {observe: 'response'}, )

    }

  checkUser(user: User):Observable<any> {
    const url = 'http://localhost:8080/start/check';
    
    return this.http.post(url, user, {observe: 'response'}, )
  }

  }

