export class User {
  name: string;
  password: string;
  isLoggedIn: boolean;
}
