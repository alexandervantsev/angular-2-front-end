import { Component, OnInit, Injectable } from '@angular/core';
import {User} from "./user";
import {Router} from '@angular/router';
import {LoginFormService} from "./login-form.service";
import { UserService } from './user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit {
  userEntity: User = {
    name: '',
    password: '',
    isLoggedIn: false
  };
  authResponse: string;

  constructor(public loginFormService: LoginFormService, private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  addUser(){
    if (this.userEntity.name === ''){
      this.authResponse = 'Enter your name';
      return;
    }
    if (this.userEntity.password === ''){
      this.authResponse = 'Enter your password';
      return;
    }
    this.loginFormService.addUser(this.userEntity).subscribe( response => {
      if (response.body.toString() === 'OK'){
        this.authResponse = 'You have been successfully registered';
      } else {
        this.authResponse = 'User with this name already exists';
      }


    });
  }

  logIn(){
    if (this.userEntity.name === ''){
      this.authResponse = 'Enter your name';
      return;
    }
    if (this.userEntity.password === ''){
      this.authResponse = 'Enter your password';
      return;
    }
    this.loginFormService.checkUser(this.userEntity).subscribe(response => {
      if (response.body.toString() === 'FOUND'){
        this.authResponse = 'OK';
        this.userService.setName(this.userEntity.name);
        this.userService.setIsLoggedIn(true);
        this.router.navigate(['/main']);
        
      } else if (response.body.toString() === 'NOT_FOUND'){
        this.authResponse = 'There is no user with this name';
      } else {
        this.authResponse = 'Invalid password';
      }
    });
  }
}
