import { Component, OnInit } from '@angular/core';
import { UserService} from '../login-form/user.service'
import {Router} from '@angular/router';
import {Point} from './point';
import {PointService} from './point.service';
import { ViewChild, ElementRef } from '@angular/core';
import { MainService } from './main.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  @ViewChild('canvas') canvasRef: ElementRef;
  private context: CanvasRenderingContext2D;
  private scale: number;
  private response: string;
  public point: Point = {
    x: 0,
    y: 0,
    r: 1,
    hit: false,
    userEntity: ""
  };
  private points: Point[];

  constructor(private userService: UserService, private router: Router, private pointService: PointService,
  private mainService: MainService) { }

  ngOnInit() {
    this.point.userEntity = this.userService.getName();
    this.scale = 80;
    this.points = [];
    this.getPoints();
  }
  ngAfterViewInit() {
    const canvas = this.canvasRef.nativeElement;
    this.context = canvas.getContext('2d');
    this.drawGraph();    
  }

  addPoint(){
    this.mainService.addPoint(this.point).subscribe( response => {
      this.response = response.body.toString();
      this.getPoints();
      
    });
    
  }
  
  getPoints(){
    this.mainService.getPoints(this.point.userEntity).subscribe( response => {
      this.points = response;
      this.drawPoints();
    });
    
  }

  drawGraph(){
    this.drawCoordinateLines();
    this.drawArea();
    this.drawPoints();
  }

  drawCoordinateLines() {
    const context = this.context;
    context.clearRect(0,0,600,600);
    context.beginPath();
    context.moveTo(300,600);
    context.lineTo(300,0);
    context.lineTo(305,5);
    context.moveTo(300,0);
    context.lineTo(295,5);
    context.moveTo(0,300);
    context.lineTo(600,300);
    context.lineTo(595,305);
    context.moveTo(600,300);
    context.lineTo(595,295);

    context.strokeStyle="black";
    context.stroke();
    context.fillStyle="black";
    context.closePath();
  }

  drawArea(){
    const context = this.context;
    const isRPositive =  this.point.r > 0 ? 1 : -1;
    const r = this.point.r*isRPositive;
    const scale = this.scale;

    context.beginPath();
    //0.25 circle
    context.arc(300 + isRPositive,300 + isRPositive, r/2*scale, (0.5 - isRPositive/2)*Math.PI, (1 - isRPositive/2)*Math.PI);
    context.lineTo(300 + isRPositive, 300 + isRPositive);
    context.moveTo((300 + isRPositive - r/2 *scale*isRPositive), 300 + isRPositive);
    context.lineTo(300 + isRPositive,(300 + isRPositive + r/2*scale*isRPositive));
    //triangle
    context.moveTo(300 - isRPositive  - r/2*scale*isRPositive, 300 + isRPositive);
    context.lineTo(300 - isRPositive, 300 + isRPositive);
    context.lineTo(300 - isRPositive, 300 + isRPositive + r*scale*isRPositive);
    context.lineTo(300 - isRPositive - r/2*scale*isRPositive, 300 + isRPositive);
    //rectangle
    if (isRPositive > 0){
      context.rect(301,299 - r* scale, r/2*scale, r*scale );
    }
    else {
      context.rect(299 - r*scale/2, 301, r/2*scale, r*scale );
    }
    context.closePath();
    context.fillStyle="lightBlue";
    context.fill();
  }

  logOut(){
    this.userService.setIsLoggedIn(false);
    this.router.navigate(['/start']);
  }

  drawPoints(){
    const ps = this.points;
    ps.forEach(p => {
      var hit = this.checkIfHit(p.x,p.y);
      this.drawPoint(p.x,p.y,hit);
    });
  }

  drawPoint(x, y, hit) {
    const context = this.context;
    const scale = this.scale;
    context.beginPath();
    if (hit) {
        context.fillStyle = "Green";
    } else {
        context.fillStyle = "Red";
    }
    context.arc(300 +x*scale,300 - y*scale, 3, 0 * Math.PI, 2 * Math.PI);
    context.fill();
  }

  checkIfHit(x, y){
    const r = this.point.r;
    var result;
    if (r > 0) result = ((x <= r/2) && (x >= 0) && (y <= r) && (y >= 0)) // rectangle
                || ((x >= 0) && (y <= 0) && (x*x + y*y <= r*r/4)) // circle
                || ((x <= 0) && (y <= 0) && (2*x + y >= -r));  // triangle
        if (r < 0) result = ((x >= r/2) && (x <= 0) && (y >= r) && (y <= 0)) // rectangle
                    || ((x <= 0) && (y >= 0) && (x*x + y*y <= r*r/4)) // circle
                    || ((x >= 0) && (y >= 0) && (2*x + y <= -r)); //triangle
        if (r==0) result = false;
        return  result;
  }

  addFromClick(event: MouseEvent){
    if (this.canvasRef != null) {
      let x, y;
      const scale = this.scale;

      x = (event.clientX - 731)/scale;
      y = (301 - event.clientY)/scale;

      this.point.x = x;

      this.point.y = y;

      this.addPoint();

    }
  }

}
