import { Injectable } from '@angular/core';
import { Point} from "./point";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from "rxjs/Observable";

@Injectable()
export class MainService {

  constructor(private http: HttpClient) { }

  addPoint(point:Point):Observable<any> {
    const url = 'http://localhost:8080/main/addPoint';
    return this.http.post(url, point, {observe: 'response'});
  }

  getPoints(username: string):Observable<any>{
    const url = 'http://localhost:8080/main/userpoint/';
    return this.http.get(url + username);
  }

}
