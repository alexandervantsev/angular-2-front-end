import {Injectable} from '@angular/core';
import {Point} from './point';
@Injectable()
export class PointService {
    point: Point = {
        x: 0,
        y: 0,
        r: 0,
        hit: false,
        userEntity: ""
    };

    getX(){
        return this.point.x;
    }

    setX(x: number){
      this.point.x = x;
    }

    getY(){
        return this.point.y;
    }

    setY(y: number){
      this.point.y = y;
    }

    getR(){
        return this.point.r;
    }

    setR(r: number){
      this.point.r = r;
    }

    getUser(): string{
        return this.point.userEntity;
    }

    setUser(name: string){
        this.point.userEntity = name;
    }

    setHit(is: boolean){
        this.point.hit = is;
    }

    ifHit() : boolean{
        return this.point.hit;
    }
}
