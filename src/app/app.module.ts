import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from './auth.guard';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { MainComponent } from './main/main.component';
import { MainService } from './main/main.service';
import { LoginFormService } from './login-form/login-form.service';
import { AppRoutingModule } from './/app-routing.module';
import { UserService } from './login-form/user.service';
import { PointService } from './main/point.service';
import { HttpClientModule } from '@angular/common/http';
import {MatSliderModule} from '@angular/material/slider';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatSliderModule
  ],
  providers: [MainService, LoginFormService, AuthGuard, UserService, PointService],
  bootstrap: [AppComponent]
})
export class AppModule { }
